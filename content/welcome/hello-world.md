+++
title = 'Hello World'
date = 2023-12-13T15:19:35+02:00
draft = false
description = ""
image = "/images/SNOW.webp"
imageBig = "/images/SNOW.webp"
categories = ["General"]
authors = ["Ejada-Team"]
avatar = "/images/ejadaTeam.webp" 
+++


# Welcome to SNOW wiki



*Following are the steps to follow to push posts to the wiki*

1. **Pull the latest repository changes to your local machine:**

 
```bash
git pull
```
1. **Add a new post using the following command (replace hacks/js-executor.md with your desired post location and title):**
```bash
hugo new content/hacks/js-executor.md
```

2. **Add the changes to the staging area:**

```bash
git add .
```

3. **Commit the changes with a descriptive message:**


```bash
git commit -m "Add js-executor post"
```
4. **Push the changes to the main branch of the remote repository:**


```bash
git push origin main
```


--- 





<!-- ![New Post in home page](/images/welcome/post-1.png)



![New Post details](/images/welcome/post-2.png) -->
