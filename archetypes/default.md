+++
title = '{{ replace .File.ContentBaseName "-" " " | title }}'
date = {{ .Date }}
draft = false
description = ""
image = "/images/SNOW.webp"
imageBig = ""

categories = ["General"]
authors = ["Ejada-Team"]
# authors = ["Ejada-Team" , "Ahmed Waleed" ,  "Hazem M. Zaki" ,"Makarious" ,"Farha" , "Mariam" , "Rawan " , "Merna" , "Ahmed Ibrahim" ]
# categories = ["Developer", "Admin", "ITSM" , "Note" , "Hack" , "UI Builder" , "Backend" , "Frontend"]
avatar = "/images/ejadaTeam.webp" 
+++

